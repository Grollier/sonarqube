FROM sonarqube:8.2-community

RUN apt-get update && apt-get install vim

COPY sonar.properties /opt/sonarqube/conf/

EXPOSE 9000